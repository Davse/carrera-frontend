# Frontend-carrera

## Pasos para la instalación (local) 

- Crear un directorio para alojar los archivos del proyecto y situarse allí
- Clonar este repositorio
- Crear un enlace simbólico desde `/var/www/html` hacia el directorio donde se alojó el proyecto `ln -s ruta-directorio-proyecto-clonado`
- Asegurarse que se encuentre instalado el servidor web y la aplicación `backend-carrera`

Para utilizar la aplicación debe utilizar el navegador web de su preferencia y acceder a `localhost`