var document = document || {},
     canvasWidth = window.innerWidth, 
     canvasHeight = 200,
     spriteWidth = 800, 
     spriteHeight = 120, 
     rows = 1, 
     cols = 4, 
     width = spriteWidth/cols, 
     height = spriteHeight/rows, 
     curFrame = 0,
     frameCount = 4,
     x=0,
     y=0, 
     srcX=0, 
     srcY=0, 
     ctx,
     character,
     img,
     Interval1,
     Interval2,
     lienzo,
Cascada = {
    inicio: function(elem){
        var canvas = document.createElement("canvas");
        lienzo = elem;
        canvas.setAttribute("id","canvas");
        ctx = canvas.getContext("2d"),
        character = new Image(),    
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        canvas.setAttribute("class","canvasOculto");
        lienzo.appendChild(canvas);
        character.src = "ImagenesSVG/Espuma.svg";
        Cascada.cascada();
        Cascada.updateFrame();
        setTimeout(function(){Interval1 = setInterval(Cascada.draw,150)},800);
        //setTimeout(function(){Cascada.finCascada()},3500);
        setTimeout(function(){canvas.setAttribute("class","canvasVisible");},800);
        return lienzo;
    },
    
     updateFrame: function(){
         curFrame = ++curFrame % frameCount;  
         srcX = curFrame * width; 
         ctx.clearRect(x,y,window.innerWidth,320);
    },
    
    draw: function(){
     Cascada.updateFrame();
     ctx.drawImage(character,srcX,srcY,width,height,x,y,window.innerWidth,320);    
    },
    
    cascada: function(){
        var rockLeft = new Image(),rockRight = new Image();
        img = new Image();
        img.setAttribute("id","cascada");
        rockLeft.setAttribute("id","rockleft");
        rockRight.setAttribute("id","rockright");
        rockLeft.src= "ImagenesSVG/rocaIzquierda.svg";
        rockRight.src= "ImagenesSVG/rocaDerecha.svg";
        img.src = "ImagenesSVG/Cascada.svg";
        rockLeft.setAttribute("class","rockLvisible");
        rockRight.setAttribute("class","rockRvisible");
        img.setAttribute("class","cascadaVisible");
        lienzo.appendChild(rockLeft);
        lienzo.appendChild(rockRight);
        lienzo.appendChild(img);
        setTimeout(function(){Interval2 = setInterval(Cascada.caidaAgua,600)},600);
    },
    
    caidaAgua: function(){ 
          img.style.transform="scaleX(-1)"; 
          setTimeout(function(){img.style.transform="scaleX(1)";},250);  
    },
    
    finCascada: function(){
        var img, rockLeft, rockRight, espuma;
        img= document.querySelector("#cascada");
        rockLeft= document.querySelector("#rockleft");
        rockRight= document.querySelector("#rockright");
        espuma= document.querySelector("#canvas");
        img.setAttribute("class","cascadaOculto");
        rockLeft.setAttribute("class","rockLoculto");
        rockRight.setAttribute("class","rockRoculto");
        espuma.setAttribute("class","canvasOculto");
        setTimeout(function(){
            espuma.style.display= "none";
            img.style.display= "none";
            rockLeft.style.display= "none";
            rockRight.style.display= "none";
            clearInterval(Interval1);
            clearInterval(Interval2);
        },1000);
    },
}