var document = document || {},
    id_partida,
    cantidad_equipos,
    dificultad,
    grillaPosiciones=new Array(),
    grillaNombres=new Array(),
    grillaAvatares=new Array(),
    fichas=new Array(),
    turnoActual,
    eContenedor,
    Dado=1,
    body,
    miServer,
    tInicio,
    tFin,
    duracion,
    htmldado='<div id="cube-hitarea"><div id="cube" style="transform: rotateX(-45deg) rotateY(50deg) rotateZ(90deg);"><svg class="side" width="100%" height="100%"> <rect fill="#DAD6CC" width="60" height="60"></rect> <circle id="cc1" fill="#F8F1E5" cx="30" cy="30" r="22"></circle> <g> <g> <circle fill="#000" cx="30" cy="30" r="5"></circle> </g> </g> </svg> <svg class="side" width="100%" height="100%"> <rect fill="#DAD6CC" width="60" height="60"></rect> <circle id="cc2" fill="#F8F1E5" cx="30" cy="30" r="22"></circle> <g> <g> <circle fill="#000" cx="44.75" cy="30" r="5"></circle> <circle fill="#000" cx="15.25" cy="30" r="5"></circle> </g> </g> </svg> <svg class="side" width="100%" height="100%"> <rect fill="#DAD6CC" width="60" height="60"></rect> <circle id="cc3" fill="#F8F1E5" cx="30" cy="30" r="22"></circle> <g> <g> <circle fill="#000" cx="52.75" cy="9.5" r="5"></circle> </g> <g> <circle fill="#000" cx="30" cy="30" r="5"></circle> </g> <g> <circle fill="#000" cx="7.25" cy="50.5" r="5"></circle> </g> </g> </svg> <svg class="side" width="100%" height="100%"> <rect fill="#dddddd" width="60" height="60"></rect> <circle id="cc4" fill="#F8F1E5" cx="30" cy="30" r="22"></circle> <g> <g> <circle fill="#000" cx="30" cy="9.5" r="5"></circle> </g> <g> <circle fill="#000" cx="52.75" cy="30" r="5"></circle> <circle fill="#000" cx="7.25" cy="30" r="5"></circle> </g> <g> <circle fill="#000" cx="30" cy="50.5" r="5"></circle> </g> </g> </svg> <svg class="side" width="100%" height="100%"> <rect fill="#cccccc" width="60" height="60"></rect> <circle id="cc5" fill="#F8F1E5" cx="30" cy="30" r="22"></circle> <g> <g> <circle fill="#000" cx="30" cy="9.5" r="5"></circle> </g> <g> <circle fill="#000" cx="52.75" cy="30" r="5"></circle> <circle fill="#000" cx="30" cy="30" r="5"></circle> <circle fill="#000" cx="7.25" cy="30" r="5"></circle> </g> <g> <circle fill="#000" cx="30" cy="50.5" r="5"></circle> </g> </g> </svg> <svg class="side" width="100%" height="100%"> <rect fill="#efefef" width="60" height="60"></rect> <circle id="cc6" fill="#F8F1E5" cx="30" cy="30" r="22"></circle> <g> <g> <circle fill="#000" cx="53.75" cy="9.5" r="5"></circle> <circle fill="#000" cx="31" cy="9.5" r="5"></circle> <circle fill="#000" cx="8.25" cy="9.5" r="5"></circle> </g><g> <circle fill="#000" cx="53.75" cy="50.5" r="5"></circle> <circle fill="#000" cx="31" cy="50.5" r="5"></circle> <circle fill="#000" cx="8.25" cy="50.5" r="5"></circle> </g> </g> </svg></div></div>',
    Tablero = {
        iniciar: function(contenedor){
                var tipo = contenedor.substr(0,1),
                varcontenedor = contenedor.substr(1,contenedor.length);
                body=document.getElementsByTagName("body")[0];
                if (tipo == "."){
                    eContenedor = document.getElementsByClassName(varcontenedor)[0];
                } else {
                    eContenedor = document.getElementById(varcontenedor);
                }
                cantidad_equipos= jsonInicion['equipos'];
                dificultad= jsonInicion['dificultad'];
                Tablero.dirServer();
        },    
        crearDado: function(){
           var dado=document.createElement("div");
            dado.setAttribute("id","dado");
            dado.innerHTML=htmldado;
            dado.addEventListener("click",function callLanzar(){Tablero.ajaxLanzar(grillaPosiciones[turnoActual]);this.removeEventListener("click", callLanzar)});
            return dado;
        },
        ventanaDado: function(){
            var fondo=document.createElement("div"),
                cajaDado=document.createElement("div"),
                cerrar=document.createElement("div");
            fondo.setAttribute("id","fondoDado");
            fondo.setAttribute("class","visible");
            cajaDado.setAttribute("id","cajaDado");
            cerrar.setAttribute("id","cerrar");
            cerrar.innerHTML="X";
            cerrar.addEventListener("click",function(){Tablero.ocultar(fondo)});
            cajaDado.appendChild(cerrar);
            cajaDado.appendChild(Tablero.crearDado());
            fondo.appendChild(cajaDado);
            return fondo;
        },

        cerrar: function(e,c){
            e.removeChild(c);
        },
        
        ocultar: function(e){
           var boton= document.createElement("div"),flecha=document.createElement("div");
            flecha.setAttribute("class","c-angularleft");
            boton.setAttribute("id","botonDado");
            boton.addEventListener("click",function(){Tablero.mostrar(e)});
            boton.appendChild(flecha);
            setTimeout(function(){body.appendChild(boton)},300);
            e.classList.remove("visible");
            e.classList.add("oculto"); 
        },
        
        mostrar: function(e){
            setTimeout(function(){Tablero.cerrar(body,document.querySelector("#botonDado"))},300);
            e.classList.remove("oculto");
            e.classList.add("visible");
        },
        
        handle_ROLL: function (dadoNumero) {     
            var rx,ry,rz;    
            Dado = dadoNumero;
            setTimeout(function(){document.querySelector("#cc"+Dado).style.fill="#538cc6"},3000);
            switch(Dado){
                case 1:
                    rx = 45;
                    ry = 90;
                    rz = 90;
                    break;
                case 2:
                    rx = -45;
                    ry = 50;
                    rz = 0;
                    break;
                case 3:
                    rx = -45;
                    ry = 225;
                    rz = -90;
                    break;
                case 4:
                    rx = -45;
                    ry = 50;
                    rz = 90;
                    break;
                case 5:
                    rx = 50;
                    ry = 0;
                    rz = 50;
                    break;
                case 6:
                    rx = 45;
                    ry = 180;
                    rz = -45;
                    break;
                default:
                    rx=-45;
                    ry=50;
                    rz=90;
                    break;
            }
        if(Dado < 7 && Dado > 0 ){ry = ry-360;rx = rx-360*3}    
        this.cube = document.getElementById('cube');
		this.cube.style['webkitTransform'] = 'rotateX(' + rx + 'deg) rotateY(' + ry + 'deg) rotateZ(' + rz + 'deg)';
		this.cube.style['MozTransform'] = 'rotateX(' + rx + 'deg) rotateY(' + ry + 'deg) rotateZ(' + rz + 'deg)';        
	},

    Puntuar: function(resPregunta,n){
        var resElegida,avanza,retrocede;
        resElegida=resPregunta['pregunta']['respuestas'][n]['id'];    
        avanza=resPregunta['new_casillero']['avanza'];
        retrocede=resPregunta['new_casillero']['retrocede'];
        Tablero.ajaxRespuesta(resElegida,avanza,retrocede);
    },        
        
    ventanaPregunta: function(resPregunta){
            var fondo=document.createElement("div"),pregunta,cajaPregunta,respuestas=new Array(),botonA,botonB,textBotonA,textBotonB,info;
            fondo.setAttribute("id","fondoPregunta");
            botonA=document.createElement("div");
            botonA.setAttribute("id","botonA");
            botonA.addEventListener("click",function(){Tablero.Puntuar(resPregunta,0)});
            botonA.addEventListener("click",function(){setTimeout(function(){Tablero.cerrar(document.getElementsByTagName("body")[0],document.querySelector('#fondoPregunta'))},700)});
            botonB=document.createElement("div");
            botonB.setAttribute("id","botonB");
            botonB.addEventListener("click",function(){Tablero.Puntuar(resPregunta,1)});
            botonB.addEventListener("click",function(){setTimeout(function(){Tablero.cerrar(document.getElementsByTagName("body")[0],document.querySelector('#fondoPregunta'))},700)});
            textBotonA=document.createElement("p");
            textBotonA.textContent="Respuesta A";
            textBotonB=document.createElement("p");
            textBotonB.textContent="Respuesta B";
            info=document.createElement("p");
            info.setAttribute("id","infoPregunta");
            info.textContent="Correcto avanza " +resPregunta['new_casillero']['avanza']+" - Incorrecto retrocede " +resPregunta['new_casillero']['retrocede'];
            cajaPregunta = document.createElement("div");
            cajaPregunta.setAttribute("id","pregunta");
            pregunta=document.createElement("p");
            pregunta.setAttribute("class","preguntas");
            pregunta.innerHTML= resPregunta['pregunta']['pregunta'];
            botonA.appendChild(textBotonA);
            botonB.appendChild(textBotonB);
            cajaPregunta.appendChild(pregunta);
                for(var j=0;j < resPregunta['pregunta']['respuestas'].length;j++){
                    respuestas[j]=document.createElement("p");
                    respuestas[j].setAttribute("class","respuestas");
                    respuestas[j].setAttribute("id","res"+j);
                    respuestas[j].innerHTML=resPregunta['pregunta']['respuestas'][j]['respuesta'];
                    cajaPregunta.appendChild(respuestas[j]);
                    cajaPregunta.appendChild(document.createElement("br"));
                }
            cajaPregunta.appendChild(botonA);
            cajaPregunta.appendChild(botonB);
            cajaPregunta.appendChild(info);
            fondo.appendChild(cajaPregunta);
            body.appendChild(fondo);
    },
    vistaTurnos: function(){
            var cajavista,textocaja;
            cajavista=document.createElement("div");
            textocaja=document.createElement("h2");
            cajavista.setAttribute("id","vistaTurno");
            cajavista.setAttribute("class","visible");
            textocaja.setAttribute("id","textoTurno");
            textocaja.textContent="Turno de "+grillaNombres[turnoActual];
            cajavista.appendChild(textocaja);
            body.appendChild(cajavista);
    },
        
    clon: function(){
        var clonAvatar;
        clonAvatar=document.querySelector("#"+grillaAvatares[turnoActual]).cloneNode("about");
        clonAvatar.setAttribute("id","clon");
        clonAvatar.style.width = "40px";
        clonAvatar.style.height = "40px";
        return clonAvatar;
    },
        
    pasarTurno: function(){
        var colores = ['rgb(0,138,230)','rgb(230, 0, 0)','rgb(255, 255, 0)','rgb(255, 133, 51)','rgb(0, 204, 204)'],contenedor = document.querySelector("#cajaDado"),dado=document.querySelector("#dado"),vista=document.querySelector("#vistaTurno");
        if (turnoActual+1 >= cantidad_equipos){
            turnoActual=0;
        }else{
            turnoActual++;
            
        }
        vista.classList.remove("visible");
        vista.classList.add("oculto");
        setTimeout(function(){
        vista.classList.remove("oculto");
        vista.classList.add("visible");
        vista.removeChild(document.querySelector("#clon"));
        vista.style.backgroundColor=colores[turnoActual];                      
        vista.insertBefore(Tablero.clon(),document.querySelector("#textoTurno"));
        document.querySelector("#textoTurno").textContent="Turno de "+grillaNombres[turnoActual];
        Tablero.cerrar(contenedor,dado);
        contenedor.appendChild(Tablero.crearDado())},1000);
    },    
    
        
    iniciarPartida: function(resIniciar){
       var tableroP;
        if (resIniciar['operation']=='success'){
            tInicio = Math.floor(Date.now() / 1000)
            tableroP = resIniciar['tablero'];
            Tablero.insertarPregunta(tableroP);
            for(var i=0;i<cantidad_equipos;i++){
              grillaPosiciones[i]=0;  
            }
            id_partida = resIniciar['id_partida'];
            turnoActual= Math.floor(Math.random() * cantidad_equipos);
            Tablero.asignaNombres();
            Tablero.vistaTurnos();
            Tablero.asignaAvatares();
            eContenedor.appendChild(Tablero.ventanaDado())
        }else{
          console.log("ERROR");  
        }
    },
    finalizarPartida: function(resFinalizar,ganador){
        if (resFinalizar['operation']=='success'){
          var fondoFin,marco,textGanador,botonReiniciar,linkBoton,textBoton;
            fondoFin = document.createElement("div");
            marco = document.createElement("div");
            textGanador = document.createElement("h1");
            botonReiniciar = document.createElement("div");
            linkBoton = document.createElement("a");
            textBoton = document.createElement("p");
            fondoFin.setAttribute("id","ganador");
            marco.setAttribute("id","cajaFin");
            textGanador.textContent="GANADOR "+ grillaNombres[ganador];
            botonReiniciar.setAttribute("id","reiniciar");
            linkBoton.setAttribute("href","index.html");
            textBoton.textContent="REINICIAR";
            linkBoton.appendChild(textBoton);
            botonReiniciar.appendChild(linkBoton);
            marco.appendChild(textGanador);
            marco.appendChild(botonReiniciar);
            fondoFin.appendChild(marco);
            body.appendChild(fondoFin);
            window.scrollTo(0,0);
        }else{
          console.log("ERROR");   
        }
    },
    animacionMov: function(nro,ficha,demora){
            return new Promise(function(resolve, reject){
            var coorX,coorY;
                setTimeout(function(){
                coorX=document.querySelector("#c"+(grillaPosiciones[ficha]+nro)).getBBox().x;
                coorY=document.querySelector("#c"+(grillaPosiciones[ficha]+nro)).getBBox().y;
                fichas[ficha].animate({x:coorX,y:coorY},500);
                grillaPosiciones[ficha]= grillaPosiciones[ficha] + nro;
                },700*demora)    
                resolve()});
    },        
        
    avanzar: function(posiciones,turno){
        var avatar= Snap("#"+grillaAvatares[turno]),fin= false;
        if(grillaPosiciones[turno]+posiciones>40){
            posiciones = 41-grillaPosiciones[turno];
            fin=true;
        }
        for(var i=0;i<posiciones;i++){
            Tablero.animacionMov(1,turno,i).then(function(){
                if(grillaPosiciones[turno]+i>40){
                    tFin=Math.floor(Date.now() / 1000);
                    duracion=tFin-tInicio;
                    Tablero.ajaxFinalizar(turno);
                }
            }).catch(function(){console.log("fallo")})        
        }   
    },
        
    retroceder: function(posiciones,turno){
      var retrocedo = new Promise(function(resolve, reject){    
        if(Tablero.animacionMov(-posiciones,turno)=='listo'){
            resolve()
        }});
      retrocedo.then(function(){    
        grillaPosiciones[turno]= grillaPosiciones[turno] - posiciones;
      }).catch(function(){console.log("fallo")})
    },
        
    cargarIcoP: function(casilla){
            return new Promise(function(resolve,reject){var insert= new Promise( function(resolve,reject){var s= Snap("#svg8"),img;
                img= Snap.load("ImagenesSVG/signopregunta.svg", function (loadedFragment) {s.append( loadedFragment);resolve("ok")});                                                     
                });
            insert.then(function(ok){
            var coorX,coorY,icono;    
            console.log(ok);
            coorX=document.querySelector("#c"+casilla).getBBox().x;
            coorY=document.querySelector("#c"+casilla).getBBox().y;
            icono = Snap("#pregunta");    
            icono.attr({x:coorX,y:coorY,height:"8px",width:"8px"}); 
            resolve("ok");    
            }).catch(function(){console.log("ERROR")})})
        },
    insertarPregunta: function(tabla){
        var icono=new Array(),coorX,coorY,primero=0;
        while(tabla[primero]['tiene_pregunta']==0){
            ++primero
        }
        if(tabla[primero]['tiene_pregunta']==1){
              Tablero.cargarIcoP(tabla[primero]['casillero']).then(function(ok){      
              for(var i=1;i<tabla.length;i++){
                  if(tabla[i]['tiene_pregunta']==1){
                      coorX=document.querySelector("#c"+tabla[i]['casillero']).getBBox().x;
                      coorY=document.querySelector("#c"+tabla[i]['casillero']).getBBox().y;
                      icono[i]=Snap("#pregunta");
                      icono[i].clone().attr({x:coorX,y:coorY,height:"8px",width:"8px"});
                  } 
             }
              }).catch()
          }
    },
        
    correcto: function(n){
        var eCorrecto = document.createElement("div"),eTexto = document.createElement("h2");
        eCorrecto.setAttribute("id","resultadoP");
        eTexto.setAttribute("id","textoCorrecto");
        eTexto.textContent="CORRECTO! +"+n;
        eCorrecto.appendChild(eTexto);
        body.appendChild(eCorrecto);
    },
    incorrecto: function(n){
        var eIncorrecto = document.createElement("div"),eTexto = document.createElement("h2");
        eIncorrecto.setAttribute("id","resultadoP");
        eTexto.setAttribute("id","textoIncorrecto");
        eTexto.textContent="INCORRECTO -"+n;
        eIncorrecto.appendChild(eTexto);
        body.appendChild(eIncorrecto);
    },
    efectoPregunta: function(){
        var contenedor= document.createElement("div");
        contenedor.setAttribute("id","iconoP");
        contenedor.innerHTML='<svg version="1.1" id="iconoPregunta" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="555.335px" height="555.335px" viewBox="0 0 555.335 555.335" style="enable-background:new 0 0 555.335 555.335;" xml:space="preserve"> <g> <path d="M330.835,440.053H227.792c-3.378,0-6.12,2.741-6.12,6.119v103.043c0,3.378,2.742,6.12,6.12,6.12h103.049 c3.378,0,6.12-2.742,6.12-6.12V446.172C336.955,442.788,334.219,440.053,330.835,440.053z"/> <path d="M422.17,47.926C386.221,16.126,337.353,0,276.929,0c-57.503,0-104.701,15.937-140.283,47.375 c-35.692,31.53-55.202,70.453-57.981,115.692c-0.196,3.225,2.148,6.053,5.355,6.45l94.609,11.732 c0.257,0.03,0.508,0.049,0.759,0.049c2.846,0,5.361-1.983,5.979-4.829c6.279-29.07,17.81-50.882,34.278-64.83 c16.383-13.88,37.136-20.918,61.684-20.918c25.459,0,45.863,6.695,60.649,19.896c14.785,13.207,21.977,28.568,21.977,46.959 c0,13.176-4.033,24.927-12.332,35.918c-3.775,4.871-16.059,17.038-52.883,46.977c-30.594,24.872-51.359,47.742-61.727,67.962 c-10.422,20.337-15.71,46.444-15.71,77.602c0,2.969,0.122,11.041,0.367,24.682c0.061,3.336,2.785,6.01,6.12,6.01h93.514 c1.641,0,3.213-0.66,4.363-1.829c1.151-1.169,1.781-2.754,1.757-4.395c-0.471-27.271,1.738-46.212,6.555-56.298 c4.847-10.146,17.938-24.131,38.911-41.573c42.943-35.704,70.282-63.281,83.587-84.309c13.402-21.193,20.202-44.009,20.202-67.81 C476.674,117.798,458.333,79.921,422.17,47.926z"/> </g> </svg>';
        body.appendChild(contenedor);
    },
        asignaNombres: function(){
            for(var i=0;i<cantidad_equipos;i++){
                grillaNombres[i]=jsonInicion['nombres']['p'+(i+1)];
            }
        },
        asignaAvatares: function(){
            for(var i=0;i<cantidad_equipos;i++){
                grillaAvatares[i]=jsonInicion['avatares']['p'+(i+1)];
            }
            Tablero.cargaAvatares();
        },
        cargaAvatares: function(){
           var s= Snap("#svg8"),img=new Array();
            for (var i=0; i < cantidad_equipos;i++){
                contador=i;
                Tablero.cargaImg(s,img,i).then(function(nro){Tablero.cargarFichas(nro);if(nro == turnoActual){
                document.querySelector("#vistaTurno").insertBefore(Tablero.clon(),document.querySelector("#textoTurno"));
                }}).catch();
            }
        },
        cargarFichas: function(i){
                switch(grillaAvatares[i]){
                    case 'ballena':
                        fichas[i] = Snap("#ballena");
                        fichas[i].attr({x:124.31+(i*8),y:3.1244*(i+1),height:"10px",width:"10px"});
                        break;
                    case 'pulpo':
                        fichas[i] = Snap("#pulpo");
                        fichas[i].attr({x:124.31+(i*8),y:3.1244*(i+1),height:"8px",width:"8px"})
                        break;
                    case 'cangrejo':
                        fichas[i] = Snap("#cangrejo");
                        fichas[i].attr({x:124.31+(i*8),y:3.1244*(i+1),height:"8px",width:"8px"})
                        break;
                    case 'pez':
                        fichas[i] = Snap("#pez");
                        fichas[i].attr({x:124.31+(i*8),y:3.1244*(i+1),height:"8px",width:"8px"})
                        break;
                    case 'caracol':
                        fichas[i] = Snap("#caracol");
                        fichas[i].attr({x:124.31+(i*8),y:3.1244*(i+1),height:"8px",width:"8px"})
                        break;    
                }
        },
        cargaImg: function(s,img,i){
            return new Promise(function(resolve,reject){
              img[i]= Snap.load("ImagenesSVG/"+grillaAvatares[i]+".svg", function (loadedFragment) {s.append( loadedFragment);resolve(i)});
            })
        },
        
        //////////////////////////////////AJAX//////////////////////////////////////
        dirServer: function() {
            var xhr = new XMLHttpRequest(),url ="Js/Server.json";
            xhr.open('GET', url, true);
            xhr.responseType = 'json';
            xhr.onload = function() {
               miServer= xhr.response;
               Tablero.ajaxIniciar();    
            };
            xhr.send();
        },  
        ajaxIniciar: function(){
            var inicio = new XMLHttpRequest(),myjson='{ "cantidad_equipos" :'+cantidad_equipos+', "dificultad" :"'+dificultad+'" }',
            method = 'POST',
            url ='http://'+miServer['ip']+':'+miServer['puerto']+'/api/iniciar';
            inicio.open(method, url, true);
            inicio.onload = function () {
              var resIniciar = this.response; 
              Tablero.iniciarPartida(resIniciar);
            };
            inicio.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
            inicio.responseType = 'json';
            inicio.send(myjson);
        },
        ajaxFinalizar: function(ganador){
            var fin = new XMLHttpRequest(),myjson='{ "id_partida" :'+id_partida+', "duracion" :"'+duracion+'" }',
            method = 'POST',
            url ='http://'+miServer['ip']+':'+miServer['puerto']+'/api/finalizar';
            fin.open(method, url, true);
            fin.onload = function () {
              var resFinalizar = this.response;
              Tablero.finalizarPartida(resFinalizar,ganador);
            };
            fin.setRequestHeader("Content-Type", "application/json; charset=UTF-8");2
            fin.responseType = 'json';
            fin.send(myjson);
        },
        ajaxLanzar: function(posicion){
            var dado = new XMLHttpRequest(),myjson='{ "id_partida" :'+id_partida+', "casillero" :'+posicion+' }',
            method = 'POST',
            url ='http://'+miServer['ip']+':'+miServer['puerto']+'/api/tirar';
            dado.open(method, url, true);
            dado.onload = function () {
              var resDado = this.response;    
              Tablero.handle_ROLL(resDado['dado']);   
              setTimeout(function(){Tablero.avanzar(resDado['dado'],turnoActual);},4000);    
              if (resDado['pregunta']!= 'null'){
                 setTimeout(function(){Tablero.efectoPregunta();},4000+(600*resDado['dado']));
                 setTimeout(function(){Tablero.cerrar(body,document.querySelector('#iconoP'));},4000+(850*resDado['dado']));  
                 setTimeout(function(){Tablero.ventanaPregunta(resDado);},4000+(950*resDado['dado'])); 
              }else{
                  setTimeout(function(){Tablero.pasarTurno();},4500+700*resDado['dado']);
              }        
            };
            dado.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
            dado.responseType = 'json';
            dado.send(myjson);
        },
        ajaxRespuesta: function(elegida,avanza,retrocede){
            var respuesta = new XMLHttpRequest(),myjson='{ "id_respuesta" :'+elegida+' }',
            method = 'POST',
            url ='http://'+miServer['ip']+':'+miServer['puerto']+'/api/contestar';
            respuesta.open(method, url, true);
            respuesta.onload = function () {
              var resRespuesta = this.response;
              if(resRespuesta['es_correcta']=='true'){
                        setTimeout(function(){Tablero.correcto(avanza);},600);
                        setTimeout(function(){Tablero.cerrar(body,document.querySelector("#resultadoP"))},2000);
                        setTimeout(function(){Tablero.avanzar(avanza,turnoActual)},600);
                    }else{
                        setTimeout(function(){Tablero.incorrecto(retrocede);},600);
                        setTimeout(function(){Tablero.cerrar(body,document.querySelector("#resultadoP"))},2000);
                        setTimeout(function(){Tablero.retroceder(retrocede,turnoActual)},600);
                    }
            setTimeout(function(){Tablero.pasarTurno()},1400);    
            };
            respuesta.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
            respuesta.responseType = 'json';
            respuesta.send(myjson);
        },
  //////////////////////////////////////////////////////////////////////////////////////////////////////
        
}