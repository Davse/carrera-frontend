var document = document || {},
    jsonInicion={"equipos":'null',
                 "dificultad":'null',
                "nombres":'null',
                "avatares": 'null'},
    nNombres={},
    nAvatares={},
    contador=0,
    personajes=['ballena','pulpo','cangrejo','pez','caracol'],
    arrayNombre= new Array(),
    arrayElegidos=new Array(),
    seleccionado='null',
    Seleccion = {
        
        inicio: function(){
            document.querySelector("#pag2").appendChild(Seleccion.seleccionPartida());
        },
        
        cantidadJugadores: function(){
          var contenedor,nPlayers= new Array(),textoPlayers= new Array(),botonReset;
            contenedor = document.createElement("section");
            contenedor.setAttribute("id","nroPlayers");
            botonReset = document.createElement("div");
            botonReset.setAttribute("class","icono-reset");
            botonReset.addEventListener("click",function(){Tablero.cerrar(document.querySelector("#campoNroJugadores"),document.querySelector("#nroPlayers"));
             if(jsonInicion['equipos'] != 'null'){
                jsonInicion['equipos']='null'; 
                document.querySelector("#campoNroJugadores").appendChild(Seleccion.cantidadJugadores())
             }else{
                document.querySelector("#campoNroJugadores").appendChild(Seleccion.cantidadJugadores()) 
             }     
            });
            contenedor.appendChild(botonReset);
            for(var i=0;i<4;i++){
                nPlayers[i]= document.createElement("div");
                textoPlayers[i]= document.createElement("p");
                nPlayers[i].setAttribute("class","equipos");
                textoPlayers[i].addEventListener("click",Seleccion.armadorJson);
                textoPlayers[i].addEventListener("click",Seleccion.bloquear);
                textoPlayers[i].textContent= i+2;
                nPlayers[i].appendChild(textoPlayers[i]); 
                contenedor.appendChild(nPlayers[i]);
            }
            return contenedor;
        },
        
        dificultad: function(){
            var contenedor,nDificultad= new Array(),textoDificultad= new Array(),botonReset,dificultades=["baja","media","dificil"];
            contenedor = document.createElement("section");
            contenedor.setAttribute("id","nroDificultad");
            botonReset = document.createElement("div");
            botonReset.setAttribute("class","icono-reset");
            botonReset.addEventListener("click",function(){Tablero.cerrar(document.querySelector("#campoDificultad"),document.querySelector("#nroDificultad"));
              if(jsonInicion['dificultad'] != 'null'){
               jsonInicion['dificultad']='null';      
               document.querySelector("#campoDificultad").appendChild(Seleccion.dificultad())
              }else{
                document.querySelector("#campoDificultad").appendChild(Seleccion.dificultad())  
              }
            });
            contenedor.appendChild(botonReset);
            for(var i=0;i<3;i++){
                nDificultad[i]= document.createElement("div");
                textoDificultad[i]= document.createElement("p");
                nDificultad[i].setAttribute("class","dificultad");
                textoDificultad[i].addEventListener("click",Seleccion.armadorJson);
                textoDificultad[i].addEventListener("click",Seleccion.bloquear);
                textoDificultad[i].textContent= dificultades[i];
                nDificultad[i].appendChild(textoDificultad[i]); 
                contenedor.appendChild(nDificultad[i]);
            }
            return contenedor;
        },
        
        seleccionPartida: function(){//main estapa 1
           var contenedor, campoNroJugadores, campoDificultad,botonSiguiente,textoBoton,tituloJugadores,tituloDificultad;
            tituloJugadores = document.createElement("h2");
            tituloDificultad = document.createElement("h2");
            tituloJugadores.textContent="Cantidad de Jugadores";
            tituloDificultad.textContent="Dificultad de Juego";
            contenedor = document.createElement("section");
            contenedor.setAttribute("id","etapa1");
            campoNroJugadores = document.createElement("div");
            campoDificultad = document.createElement("div");
            botonSiguiente = document.createElement("div");
            botonSiguiente.setAttribute("id","botonSiguiente");
            textoBoton= document.createElement("span");
            textoBoton.textContent="Siguiente";
            botonSiguiente.appendChild(textoBoton);
            botonSiguiente.addEventListener("click",function(){Seleccion.etapa2()});
            contenedor.setAttribute("id","seleccionPartida");
            campoNroJugadores.setAttribute("id","campoNroJugadores");
            campoDificultad.setAttribute("id","campoDificultad");
            campoNroJugadores.appendChild(Seleccion.cantidadJugadores());
            campoDificultad.appendChild(Seleccion.dificultad());
            contenedor.appendChild(tituloJugadores);
            contenedor.appendChild(campoNroJugadores);
            contenedor.appendChild(tituloDificultad);
            contenedor.appendChild(campoDificultad);
            contenedor.appendChild(botonSiguiente);
            return contenedor;
        },
        
        etapa2: function(){
            var mensaje,h1,cerrar;
            if(jsonInicion['equipos'] == 'null' || jsonInicion['dificultad'] == 'null'){
                mensaje = document.createElement("div");
                h1 = document.createElement("h1");
                mensaje.setAttribute("id","mensajeError");
                h1.textContent="Falto seleccionar una opcion";
                cerrar=document.createElement("div");
                cerrar.setAttribute("id","cerrar");
                cerrar.innerHTML="X";
                cerrar.addEventListener("click",function(){Tablero.cerrar(document.querySelector("#pag2"),mensaje)});
                mensaje.appendChild(cerrar);
                mensaje.appendChild(h1);
                document.querySelector("#pag2").appendChild(mensaje);
            }else{
                Tablero.cerrar(document.querySelector("#pag2"),document.querySelector("#seleccionPartida"));
                Paginador.crearPag1(document.querySelector("body"));
                var promesa = new Promise(function(resolve,reject){setTimeout(function(){
                 document.querySelector("#pag2").appendChild(Seleccion.seleccionJugador())
                },2500);resolve(document.querySelector("body"))});
                promesa.then(function(contenedorMain){Paginador.cerrarCascada(contenedorMain);}).catch()
            }
        },
        
        seleccionNombre: function(){
            var contenedor,inputNombre, label;
            contenedor = document.createElement("div");
            inputNombre = document.createElement("input");
            label = document.createElement("label");
            contenedor.setAttribute("id","inputNombre");
            inputNombre.setAttribute("type","text");
            inputNombre.setAttribute("id","nombreJugador");
            inputNombre.setAttribute("required","required");
            label.setAttribute("for","nombreJugador");
            label.innerHTML="Ingresa un Nombre ";
            contenedor.appendChild(label);
            contenedor.appendChild(inputNombre);
            return contenedor;
        },
        
        seleccionAvatares: function(){
            var contenedor, marco= new Array(), imagenes= new Array(), avatares=['ImagenesSVG/ballena.svg','ImagenesSVG/pulpo.svg','ImagenesSVG/cangrejo.svg', 'ImagenesSVG/pez.svg', 'ImagenesSVG/caracol.svg'],botonReset;
            contenedor = document.createElement("div");
            botonReset = document.createElement("div");
            botonReset.setAttribute("class","icono-reset");
            botonReset.addEventListener("click",function(){Seleccion.resetAvatar()});
            contenedor.setAttribute("id","contenedorAvatares");
            contenedor.appendChild(botonReset);
            for(var i=0; i < 5; i++){
                marco[i]= document.createElement("div");
                imagenes[i]= document.createElement("img");
                marco[i].setAttribute("id",i+"p");
                marco[i].setAttribute("class","cajaAvatar");
                imagenes[i].setAttribute("src",avatares[i]);
                imagenes[i].setAttribute("class","personajes");
                imagenes[i].addEventListener("click",Seleccion.elegido);
                marco[i].appendChild(imagenes[i]);
                contenedor.appendChild(marco[i]);
            }
            return contenedor;
        },
        
        resetAvatar: function(){
            if(seleccionado != 'null'){
                document.querySelector("#contenedorAvatares").removeChild(document.querySelector("#contenedorAvatares > .sombra"));
                seleccionado.parentElement.classList.remove("avatarElegido");
                seleccionado.addEventListener("click",Seleccion.elegido);
                seleccionado = 'null';
            }    
        },
        
        elegido: function(elemento){
            var sombra;
            seleccionado = elemento.target;
            sombra = document.createElement("div");
            sombra.setAttribute("class","sombra");
            document.querySelector("#contenedorAvatares").appendChild(sombra);
            elemento.target.parentElement.classList.add("avatarElegido");
            elemento.target.removeEventListener("click",Seleccion.elegido);
        },
        
        seleccionJugador: function(){//main estapa 2
            var contenedor,campoNombre,campoAvatares,botonSiguiente,textoBoton,jugador;
            contenedor= document.createElement("div");
            contenedor.setAttribute("id","etapa2");
            campoNombre= document.createElement("div");
            campoAvatares = document.createElement("div");
            botonSiguiente = document.createElement("div");
            jugador = document.createElement("h2");
            jugador.setAttribute("id","selectJugador");
            jugador.textContent= "JUGADOR " + (contador+1);
            botonSiguiente.setAttribute("id","botonSelect");
            botonSiguiente.setAttribute("class","jugar");
            botonSiguiente.addEventListener("click",function pasar(){Seleccion.siguienteJugador()});
            textoBoton = document.createElement("p");
            textoBoton.textContent="Siguiente";
            textoBoton.setAttribute("id","textoBoton");
            campoNombre.setAttribute("id","campoNombre");
            campoAvatares.setAttribute("id","campoAvatares");
            campoNombre.appendChild(Seleccion.seleccionNombre());
            campoAvatares.appendChild(Seleccion.seleccionAvatares());
            botonSiguiente.appendChild(textoBoton);
            contenedor.appendChild(jugador);
            contenedor.appendChild(campoNombre);
            contenedor.appendChild(campoAvatares);
            contenedor.appendChild(botonSiguiente);
            return contenedor;
        },
        
        siguienteJugador: function(){
           var arrayP= new Array(),mensaje,h1,cerrar,bloqueado=document.createElement("div"),json;
        if(!(contador < jsonInicion["equipos"]-1)){
                if(!(document.querySelector("#nombreJugador").value) || seleccionado == 'null'){
                        mensaje = document.createElement("div");
                        h1 = document.createElement("h1");
                        mensaje.setAttribute("id","mensajeError");
                        h1.textContent="Falto seleccionar una opcion";
                        cerrar=document.createElement("div");
                        cerrar.setAttribute("id","cerrar");
                        cerrar.innerHTML="X";
                        cerrar.addEventListener("click",function(){Tablero.cerrar(document.querySelector("#etapa2"),mensaje)});
                        mensaje.appendChild(cerrar);
                        mensaje.appendChild(h1);
                        document.querySelector("#etapa2").appendChild(mensaje);
                 }else{
                    document.querySelector("#etapa2").removeChild(document.querySelector("#botonSelect"));
                    arrayNombre[contador] = document.querySelector("#nombreJugador").value;
                    arrayElegidos[contador] = personajes[parseInt(seleccionado.parentElement.id)];
                    document.querySelector("#contenedorAvatares").removeChild(document.querySelector("#contenedorAvatares > .sombra"));
                    bloqueado.setAttribute("class","sombra");
                    seleccionado.parentElement.appendChild(bloqueado);
                    seleccionado='null';
                    json = new Promise(function(resolve,reject){
                        for(var i=0; i < jsonInicion["equipos"]; i++){
                            arrayP[i]= "p"+(i+1);
                            nNombres[arrayP[i]]= arrayNombre[i];
                            nAvatares[arrayP[i]]= arrayElegidos[i];
                            Seleccion.armadorJson;

                        }
                        resolve();
                    });
                    json.then(function(){jsonInicion['nombres']=nNombres;jsonInicion['avatares']=nAvatares}).catch();
                    Paginador.crearPag1(document.querySelector("body"));
                    var promesa = new Promise(function(resolve,reject){Paginador.crearPag3(document.querySelector("body"));resolve();});
                     promesa.then(function(){Paginador.cerrarCascada(document.querySelector("body"))}).catch()}
             }else{    
                if(!(document.querySelector("#nombreJugador").value) || seleccionado == 'null'){
                        mensaje = document.createElement("div");
                        h1 = document.createElement("h1");
                        mensaje.setAttribute("id","mensajeError");
                        h1.textContent="Falto seleccionar una opcion";
                        cerrar=document.createElement("div");
                        cerrar.setAttribute("id","cerrar");
                        cerrar.innerHTML="X";
                        cerrar.addEventListener("click",function(){Tablero.cerrar(document.querySelector("#etapa2"),mensaje)});
                        mensaje.appendChild(cerrar);
                        mensaje.appendChild(h1);
                        document.querySelector("#etapa2").appendChild(mensaje);
                 }else{
                         arrayNombre[contador] = document.querySelector("#nombreJugador").value;
                         arrayElegidos[contador] = personajes[parseInt(seleccionado.parentElement.id)];    
                         contador++;
                         document.querySelector("#selectJugador").textContent = "JUGADOR " + (contador+1);
                         document.querySelector("#campoNombre").removeChild(document.querySelector("#inputNombre"));
                         document.querySelector("#campoNombre").appendChild(Seleccion.seleccionNombre());
                         document.querySelector("#contenedorAvatares").removeChild(document.querySelector("#contenedorAvatares > .sombra"));
                         bloqueado.setAttribute("class","sombra");
                         seleccionado.parentElement.appendChild(bloqueado);
                         seleccionado = 'null';
                         if(!(contador < jsonInicion["equipos"]-2)){
                            textoBoton.textContent="Jugar";
                         }

                     }
            }
        },
        
        armadorJson: function(elemento){
            var tipoDato = elemento.target.parentElement.className,valor = elemento.target.textContent;
            elemento.target.parentElement.classList.add("elegido");
            elemento.target.removeEventListener("click",Seleccion.armadorJson);
            switch(tipoDato){
                case 'equipos':
                    jsonInicion["equipos"] = valor;
                    break;
                case 'dificultad':
                    jsonInicion["dificultad"] =  valor;
                    break;
                case 'jugar':
                    jsonInicion["nombres"] = nNombres;
                    jsonInicion["avatares"] = nAvatares;
                    Paginador.crearPag3();
                    break;  
            }
        },
        
        bloquear: function(elemento){
           var array = document.getElementsByName("."+elemento.target.parentElement.className),clones= new Array(),sombra;
            sombra = document.createElement("div");
            sombra.setAttribute("class","sombra");
            elemento.target.removeEventListener("click",Seleccion.bloquear);
            if( elemento.target.parentElement.getAttribute("class") == "equipos elegido" ){
                document.querySelector("#nroPlayers").appendChild(sombra);
            }else{
                document.querySelector("#nroDificultad").appendChild(sombra);
            }
            for(var i=0;i< array.length;i++){
                clones[i]=array[i].cloneNode(true);
                array[i].parentNode.replaceChild(clones[i],array[i]); // clono y reemplazo sin eventlistener
            }
        },
        
    }